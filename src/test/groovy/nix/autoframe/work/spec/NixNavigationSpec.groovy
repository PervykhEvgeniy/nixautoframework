package nix.autoframe.work.spec

import geb.spock.GebReportingSpec
import nix.autoframe.work.page.BlogPage
import nix.autoframe.work.page.StartPage

class NixNavigationSpec extends GebReportingSpec {

    def "Navigate to blog page"(){
        when:
           to StartPage
            println"something new"
        and:
            "User navigates to Start page"()
        then:
            at BlogPage
    }
}
